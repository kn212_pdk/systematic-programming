﻿namespace DiskInfo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.button_spectate = new System.Windows.Forms.Button();
            this.button_update = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // webBrowser
            // 
            this.webBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser.Location = new System.Drawing.Point(14, 61);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(774, 377);
            this.webBrowser.TabIndex = 1;
            // 
            // button_spectate
            // 
            this.button_spectate.Location = new System.Drawing.Point(14, 12);
            this.button_spectate.Name = "button_spectate";
            this.button_spectate.Size = new System.Drawing.Size(129, 43);
            this.button_spectate.TabIndex = 2;
            this.button_spectate.Text = "Chose folder and spectate";
            this.button_spectate.UseVisualStyleBackColor = true;
            this.button_spectate.Click += new System.EventHandler(this.button_spectate_Click);
            // 
            // button_update
            // 
            this.button_update.Location = new System.Drawing.Point(160, 12);
            this.button_update.Name = "button_update";
            this.button_update.Size = new System.Drawing.Size(126, 43);
            this.button_update.TabIndex = 3;
            this.button_update.Text = "Update disks";
            this.button_update.UseVisualStyleBackColor = true;
            this.button_update.Click += new System.EventHandler(this.button_update_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button_update);
            this.Controls.Add(this.button_spectate);
            this.Controls.Add(this.webBrowser);
            this.MinimumSize = new System.Drawing.Size(818, 497);
            this.Name = "Form1";
            this.Text = "Disk Info";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.Button button_spectate;
        private System.Windows.Forms.Button button_update;
    }
}

