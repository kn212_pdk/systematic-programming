﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Management;
using System.Management.Instrumentation;
using Microsoft.VisualBasic.FileIO;

namespace DiskInfo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            DriveInfo[] driveInfo = DriveInfo.GetDrives();
            GetDiskInfo(driveInfo, webBrowser);
        }

        public static void GetDiskInfo(DriveInfo[] driveInfo, WebBrowser webBrowser)
        {
            int step = 30;
            string html = "<style> table, th, td { border: 1px solid black; border-collapse: collapse; }" +
                " th, td { background - color: #ececec; padding: 5px; text-align: left } </style>";
            foreach (DriveInfo driver in driveInfo)
            {
                html += generateTable(driver);
            }

            ManagementObjectSearcher Search = new ManagementObjectSearcher("Select TotalPhysicalMemory From Win32_ComputerSystem");
            double Ram_Bytes = 0;
            foreach (ManagementObject Mobject in Search.Get())
            {
                Ram_Bytes = (Convert.ToDouble(Mobject["TotalPhysicalMemory"]));
            }

            DirectoryInfo info = new DirectoryInfo(Environment.SystemDirectory);
            double totalSystemSize = info.EnumerateFiles().Sum(file => file.Length);
            info = new DirectoryInfo(SpecialDirectories.Temp);
            double totalTempSize = info.EnumerateFiles().Sum(file => file.Length);
            var currentDirectory = Directory.GetCurrentDirectory();

            html += $"<h3>RAM Size in GiB: {Math.Round(Ram_Bytes / 1073741824, 2)}<br/>" +
                $"PC name: {Environment.MachineName}<br/>" +
                $"User: { Environment.UserName}<br/>" +
                $"System folder: {Environment.SystemDirectory} with total size of {Math.Round(totalSystemSize / 1073741824, 2)} GiB<br/>" +
                $"Temporary folder: {SpecialDirectories.Temp} with total size of {Math.Round(totalTempSize / 1073741824, 2)} GiB<br/>" +
                $"Current folder: {currentDirectory}</h3>";

            webBrowser.DocumentText = html;
        }

        public static string generateTable(DriveInfo driver)
        {
            string html = "<table style='margin-bottom: 20px'>";
            html += $"<tr><td>Name</td><td>{driver.Name}</td></tr>";
            html += $"<tr><td>Type</td><td>{driver.DriveType}</td></tr>";
            html += $"<tr><td>Format</td><td>{driver.DriveFormat}</td></tr>";
            html += $"<tr><td>Total free space in Bytes</td><td>{driver.TotalFreeSpace}</td></tr>";
            html += $"<tr><td>Unavilable space in Bytes</td><td>{driver.TotalSize - driver.TotalFreeSpace}</td></tr>";
            html += $"<tr><td>Total size in Bytes</td><td>{driver.TotalSize}</td></tr>";
            html += "</table>";
            return html;
        }
        
        private void button_update_Click(object sender, EventArgs e)
        {
            DriveInfo[] driveInfo = DriveInfo.GetDrives();
            GetDiskInfo(driveInfo, webBrowser);
        }

        private void button_spectate_Click(object sender, EventArgs e)
        {
            string folder = "";
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Choose folder to watch";
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                folder = fbd.SelectedPath;
            }
            var watcher = new FileSystemWatcher(folder, "*.*");
            watcher.IncludeSubdirectories = true;
            watcher.EnableRaisingEvents = true;
            watcher.Changed += Changing;
            watcher.Created += Creating;
            watcher.Deleted += Deleting;
            watcher.Renamed += Renaming;
            watcher.Error += Failing;
        }

        public static void Logify(string log)
        {
            string log_path = Directory.GetCurrentDirectory() + "\\lab.log";
            if (File.Exists(log_path))
            {
                string content = File.ReadAllText(log_path);
                File.AppendAllText("lab.log", $"{log}\n");
            }
            else
            {
                File.WriteAllText("lab.log", log);
            }
        }

        private static void Changing(object sender, FileSystemEventArgs e)
        {
            if (e.ChangeType != WatcherChangeTypes.Changed)
                return;

            Logify($"File was changed at: {e.FullPath}");
        }

        private static void Creating(object sender, FileSystemEventArgs e)
        {
            Logify($"File was created at: {e.FullPath}");
        }

        private static void Deleting(object sender, FileSystemEventArgs e)
        {
            Logify($"File was deleted at: {e.FullPath}");
        }

        private static void Renaming(object sender, RenamedEventArgs e)
        {
            Logify($"File at: {e.OldFullPath} was renamed to: {e.FullPath}");
        }

        private static void Failing(object sender, ErrorEventArgs e)
        {
            ShowException(e.GetException());
        }

        private static void ShowException(Exception ex)
        {
            if (ex != null)
            {
                Console.WriteLine($"Message: {ex.Message}");
                Console.WriteLine("Stacktrace:");
                Console.WriteLine(ex.StackTrace);
                Console.WriteLine();
                ShowException(ex.InnerException);
                Logify(ex.Message);
            }
        }
    }
}
