import zipfile
import socket
import struct
import sys
import datetime
import win32api
import os
import psutil
import smtplib
import subprocess

current_dir = os.getcwd()


def log_check(arg):
    if not os.path.exists(arg):
        with open(arg, 'a', encoding="utf-8") as file:
            file.writelines([str(datetime.datetime.now()), f"\nФайл з ім’ям {arg} відкрито або створено\n"])


def ntp_time_get(server='0.ua.pool.ntp.org'):
    REF_TIME_1970 = 2208988800  # Reference time
    client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    data = b'\x1b' + 47 * b'\0'
    client.sendto(data, (server, 123))
    data, address = client.recvfrom(1024)
    tm = 0
    if data:
        tm = struct.unpack('!12I', data)[10]
        tm -= REF_TIME_1970
    return tm


def ntp_time_set(arg):
    epoch_time = ntp_time_get()

    utc_time = datetime.datetime.utcfromtimestamp(epoch_time)
    win32api.SetSystemTime(utc_time.year, utc_time.month, utc_time.weekday(), utc_time.day, utc_time.hour,
                          utc_time.minute, utc_time.second, 0)
    # Local time is obtained using fromtimestamp()
    local_time = datetime.datetime.fromtimestamp(epoch_time)
    file = open(arg, "a", encoding="utf-8")
    file.writelines("Оновлений час: " + local_time.strftime("%Y-%m-%d %H:%M") + "\n")
    file.close()


def log_processes(arg):
    processes = []
    for process in psutil.process_iter():
        try:
            # Get process name & pid from process object.
            process_name = process.name()
            process_id = process.pid
            processes.append(f"{process_name} <-> {process_id}\n")
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    file = open(arg, "a", encoding="utf-8")
    file.writelines(processes)
    file.close()


def kill_process(log, process):
    file = open(log, "a", encoding="utf-8")
    for proc in psutil.process_iter():
        # Check whether the process name matches
        try:
            if proc.name() == process:
                proc.kill()
                file.writelines(f"Процес {process} було завершено\n")
        except:
            file.writelines(f"Процес {process} недоступний\n")
    file.close()


def delete_tmp_files(log, dir):
    file = open(log, "a", encoding="utf-8")
    files = [f for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))]
    counter = 0
    for f in files:
        if f[-4:] == ".tmp" or f[:4] == "temp":
            os.remove(dir + "/" + f)
            counter += 1
            file.writelines(f"Файл {f} видалено, всього видалено - {counter}\n")
    file.close()


def move_file_to(log, file, dir, dst):
    os.chdir(dir)
    os.replace(file, dst + f"/{file}")
    f = open(log, "a", encoding='utf-8')
    f.writelines(f"Файл {file} переміщено до {dst}\n")
    f.close()


def zip_files(log, dir, move_to="C://"):
    os.chdir(dir)
    file_name = str(datetime.datetime.now()).replace(":", ".") + ".zip"
    zip_file = zipfile.ZipFile(file_name, "w")
    for root, dirs, files in os.walk(dir):
        for file in files:
            if dir in os.path.abspath(file):
                zip_file.write(os.path.join(root, file))
    zip_file.close()
    move_file_to(log, file_name, dir, move_to)
    file = open(log, "a", encoding="utf-8")
    file.writelines(f"Файли в {dir} заархівовано\n")
    file.close()


def check_yesterday_zip(log, path):
    yesterday = datetime.datetime.today() - datetime.timedelta(days=1)
    os.chdir(path)
    counter = 0
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    file = open(log, "a", encoding="utf-8")

    for f in files:
        if f[-4:] == ".zip":
            j = f.split(" ")
            j = j[0]
            j.split("-")
            if str(yesterday.year) == j[0] and str(yesterday.month) == j[1] and str(yesterday.day) == j[3]:
                file.writelines("Присутній архів з вчора\n")
                counter += 1
    if counter == 0:

        server = 'smtp.gmail.com'
        user = 'frankotyk@gmail.com'
        password = '1vanVesely'
        recipient = 'ipz191_lnv@student.ztu.edu.ua'

        subject = "Lab2 архіву за вчора не існує (завдання 12)"
        body = f"За шляхом {path} архівів за {yesterday} не найдено"
        message = f'Subject: {subject}\n{body}'

        mail = smtplib.SMTP_SSL(server)
        mail.login(user, password)
        mail.sendmail(user, recipient, message.encode('utf-8'))
        mail.quit()

        file.writelines(f"За шляхом {path} архівів за {yesterday} не найдено. Повідомлення на email відправлено")
    file.close()


def check_30_days_zip(log, path):
    yesterday = datetime.datetime.today() - datetime.timedelta(days=30)
    os.chdir(path)
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
    file = open(log, "a", encoding="utf-8")

    for f in files:
        if f[-4:] == ".zip":
            j = f.split(".zip")
            j = j[0].replace(".", ":")
            try:
                j = datetime.datetime.strptime(j[:len(j) - 7] + "." + j[-6:])
                if j < yesterday:
                    os.remove(path + "/" + f)
                    file.writelines(f"Файл {f} було видалено, оскільки він старший 30 днів\n")
            except:
                pass
    file.close()


def check_internet_connection(log, host='https://google.com'):
    import urllib.request
    file = open(log, "a", encoding="utf-8")
    try:
        urllib.request.urlopen(host)  # Python 3.x
        file.writelines("Internet-з’єднання присутнє\n")
    except:
        file.writelines("Internet-з’єднання відсутнє\n")
    file.close()


def shutdown_by_ip(log, ip):
    file = open(log, 'a', encoding="utf-8")
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect((ip, 80))
        subprocess.run(f"shutdown -m \\\\{ip} -f -t 0")
        file.writelines(f"Комп’ютер {s.getsockname()[0]} вимкнено\n")
    except:
        file.writelines(f"Комп’ютер з адресою {ip} не знайдено\n")
    file.close()


def get_lan_addresses(log):
    file = open(log, 'a', encoding="utf-8")
    from netifaces import interfaces, ifaddresses, AF_INET
    ip_on = []
    for interface in interfaces():
        addresses = [i['addr'] for i in ifaddresses(interface).setdefault(AF_INET, [{'addr': 'No IP addr'}])]
        file.writelines(' '.join(addresses))
        ip_on.append(addresses)
    file.close()
    return ip_on


def check_ip_on(log, ip_on):
    from pathlib import Path
    file = open(log, "a", encoding="utf-8")
    ip_on_file = str(Path(current_dir+'/ipon.txt').read_text())
    ip_on_file = ip_on_file.split("\n")
    for ip in ip_on:
        if ip in ip_on_file:
            pass
        else:
            server = 'smtp.gmail.com'
            user = 'frankotyk@gmail.com'
            password = '1vanVesely'
            recipient = 'ipz191_lnv@student.ztu.edu.ua'

            subject = "Lab2 ip не знайдено (завдання 18)"
            body = f"IP {ip} у файлі ipon.txt не найдено\n"
            message = f'Subject: {subject}\n{body}'

            mail = smtplib.SMTP_SSL(server)
            mail.login(user, password)
            mail.sendmail(user, recipient, message.encode('utf-8'))
            mail.quit()

            file.writelines(f"IP {ip} у файлі ipon.txt не знайдено\n")
    file.close()


def disk_usage(log):
    disk = psutil.disk_usage('/')
    file = open(log, "a", encoding="utf-8")
    file.writelines([f"Total: {disk.total / (2 ** 30)}GiB",
                     f"In use: {disk.used / (2 ** 30)} GiB",
                     f"Free: {disk.free / (2 ** 30)} GiB\n"])
    file.close()


def systeminfo_write():
    file = open(f"systeminfo+{str(datetime.datetime.now()).replace(':','.')}.txt", 'w', encoding="utf-8")
    file.writelines(subprocess.getoutput("systeminfo"))
    file.close()


def size_info(log, arg):
    if os.path.getsize(log) > int(arg):
        f = open(log, "a", encoding="utf-8")
        f.writelines("\nfile bigger then border you put in")
        f.close()

        server = 'smtp.gmail.com'
        user = 'frankotyk@gmail.com'
        password = '1vanVesely'
        recipient = 'ipz191_lnv@student.ztu.edu.ua'

        subject = "Завеликий файл (завдання 19)"
        body = f"Файл логу більший за {arg}\n"
        message = f'Subject: {subject}\n{body}'

        mail = smtplib.SMTP_SSL(server)
        mail.login(user, password)
        mail.sendmail(user, recipient, message.encode('utf-8'))
        mail.quit()


if __name__ == "__main__":
    args = sys.argv
    log_check(args[1])
    ntp_time_set(args[1])
    log_processes(args[1])
    kill_process(args[1], args[3])
    delete_tmp_files(args[1], args[2])
    zip_files(args[1], args[2], args[4])
    check_yesterday_zip(args[1], args[4])
    check_30_days_zip(args[1], args[4])
    check_internet_connection(args[1])
    shutdown_by_ip(args[1], args[5])
    check_ip_on(args[1], get_lan_addresses(args[1]))
    disk_usage(args[1])
    systeminfo_write()
    size_info(args[1], args[6])
