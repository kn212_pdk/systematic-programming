﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4
{
    public partial class Form1 : Form
    {
        string DocumentHtml = "<table border=1><tr><th>Name</th><th>Value</th></tr>";

        public Form1()
        {
            InitializeComponent();

            string html = BuildStartupHtml();
            getRegKey(Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Schedule\TaskCache\Tree\"));

            html += DocumentHtml + "</table>";
            webBrowser.DocumentText = html;
        }

        private static string BuildStartupHtml()
        {
            string html = "<h2>Globally</h2><ul>";

            List<string> programs = GetAutoStartupProgramsGlobally();

            foreach (string program in programs)
            {
                html += $"<li>{program}</li>";
            }

            html += "</ul><h2>Locally</h2><ul>";

            programs = GetAutoStartupProgramsLocally();

            foreach (string program in programs)
            {
                html += $"<li>{program}</li>";
            }

            html += "</ul>";

            return html;
        }

        private static List<string> GetAutoStartupProgramsGlobally()
        {
            List<string> programs = new List<string>();
            RegistryKey regKey = Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run");

            foreach (string program in regKey.GetValueNames())
            {
                programs.Add(regKey.GetValue(program).ToString());
            }

            return programs;
        }

        private static List<string> GetAutoStartupProgramsLocally()
        {
            List<string> programs = new List<string>();
            RegistryKey regKey = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run");

            foreach (string program in regKey.GetValueNames())
            {
                programs.Add(regKey.GetValue(program).ToString());
            }

            return programs;
        }

        private void buttonAddToStartup_Click(object sender, EventArgs e)
        {
            using (RegistryKey regKey = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run", true))
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "Exe Files (.exe)|*.exe|All Files (*.*)|*.*";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    regKey.SetValue($"New Programm", $"\"{openFileDialog.FileName}\"");
                }
            }
            string html = BuildStartupHtml();
            getRegKey(Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Schedule\TaskCache\Tree\"));

            html += DocumentHtml + "</table>";

            webBrowser.DocumentText = html;
        }

        private string getTasksNames(RegistryKey regKey)
        {
            string[] tasksNames = regKey.GetValueNames();
            if (tasksNames == null || tasksNames.Length <= 0) // no values?)
                return "";

            string html = "";
            foreach (string name in tasksNames)
            {
                object obj = regKey.GetValue(name);
                if (obj != null)
                    html += $"<tr><td>{regKey.Name}</td><td>{name}</td></tr>";
            }

            return html;
        }

        private void getRegKey(RegistryKey regKey)
        {
            try
            {
                string[] names = regKey.GetSubKeyNames(); //means deeper folder

                if (names == null || names.Length <= 0) //has no more subkeys
                {
                    DocumentHtml += getTasksNames(regKey);
                }
                foreach (string keyname in names) //has more subkeys
                {
                    using (RegistryKey key = regKey.OpenSubKey(keyname))
                    {
                        DocumentHtml += getTasksNames(key);
                        getRegKey(key);
                    }
                }
            }
            catch
            {
                DocumentHtml += "<h1 style='color: red'>error</h1>";
            }
        }

        private void buttonCopyRegistry_Click(object sender, EventArgs e)
        {
            string regTemplate = @"Windows Registry Editor Version 5.00\r\n[{0}]\r\n""{1}""=""{2}""";
            RegistryKey key = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run");
            var k = key.GetValueNames()[0];
            var r = key.GetValue(k);
            string regFileContent = string.Format(regTemplate, "Software\\Microsoft\\Windows\\CurrentVersion\\Run", k, r);
            File.WriteAllText("Run_Copy.reg", regFileContent);
        }
    }
}
