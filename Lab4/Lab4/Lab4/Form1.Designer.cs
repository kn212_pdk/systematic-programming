﻿namespace Lab4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.buttonAddToStartup = new System.Windows.Forms.Button();
            this.buttonCopyRegistry = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(800, 450);
            this.webBrowser1.TabIndex = 0;
            // 
            // webBrowser
            // 
            this.webBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser.Location = new System.Drawing.Point(0, 59);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(800, 391);
            this.webBrowser.TabIndex = 1;
            // 
            // buttonAddToStartup
            // 
            this.buttonAddToStartup.Location = new System.Drawing.Point(12, 12);
            this.buttonAddToStartup.Name = "buttonAddToStartup";
            this.buttonAddToStartup.Size = new System.Drawing.Size(190, 33);
            this.buttonAddToStartup.TabIndex = 2;
            this.buttonAddToStartup.Text = "Add programm to startup";
            this.buttonAddToStartup.UseVisualStyleBackColor = true;
            this.buttonAddToStartup.Click += new System.EventHandler(this.buttonAddToStartup_Click);
            // 
            // buttonCopyRegistry
            // 
            this.buttonCopyRegistry.Location = new System.Drawing.Point(246, 16);
            this.buttonCopyRegistry.Name = "buttonCopyRegistry";
            this.buttonCopyRegistry.Size = new System.Drawing.Size(215, 28);
            this.buttonCopyRegistry.TabIndex = 3;
            this.buttonCopyRegistry.Text = "Copy Registry folder to file";
            this.buttonCopyRegistry.UseVisualStyleBackColor = true;
            this.buttonCopyRegistry.Click += new System.EventHandler(this.buttonCopyRegistry_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buttonCopyRegistry);
            this.Controls.Add(this.buttonAddToStartup);
            this.Controls.Add(this.webBrowser);
            this.Controls.Add(this.webBrowser1);
            this.Name = "Form1";
            this.Text = "Lab4";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.Button buttonAddToStartup;
        private System.Windows.Forms.Button buttonCopyRegistry;
    }
}

