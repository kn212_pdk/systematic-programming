﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using System.Globalization;

namespace Task_Bonus
{
    class Program
    {
        protected static string mutexId = String.Format("{{{0}}}", "NazariiLeut");
        protected static Mutex mutex = new Mutex(false, mutexId);

        public static void WriteData(List<int> list)
        {
            string[] res = list.Select(x => x.ToString()).ToArray();
            File.WriteAllLines("../../../data.dat", res);
        }

        public static List<int> ReadData()
        {
            var lines = File.ReadLines("../../../data.dat", Encoding.UTF8);
            List<int> list = new List<int>();

            foreach (var l in lines)
            {
                if (!int.TryParse(l, out int line))
                    break;

                list.Add(line);
            }

            return list;
        }

        public static void InsertionSort(List<int> list)
        {
            int t, i, j;
            for (i = 1; i < list.Count; i++)
            {
                t = list[i];
                j = i - 1;
                while ( j >= 0 && t > list[j])
                {
                    list[j + 1] = list[j];
                    --j;
                }
                list[j + 1] = t;

                mutex.WaitOne();
                WriteData(list);
                mutex.ReleaseMutex();
                Task.Delay(1000).Wait();
            }
        }

        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            Console.OutputEncoding = Encoding.Unicode;

            Console.WriteLine("Натисніть пробіл");

            while (true)
            {
                if (Console.ReadKey().Key == ConsoleKey.Spacebar)
                    break;
            }

            mutex.WaitOne();
            List<int> list = ReadData();
            mutex.ReleaseMutex();
            Console.WriteLine("\nДані завантажено");

            InsertionSort(list);
            Console.WriteLine("Роботу завершено");
        }
    }
}
