﻿using System;
using System.Linq;
using System.IO;
using System.Threading;

namespace Task_1._3
{
    class Program
    {
        public static void FileWriteLineByLine(int[] arr)
        {
            string[] res = arr.Select(x => x.ToString()).ToArray();

            File.WriteAllLines("../../../data.dat", res);
        }

        static void Main(string[] args)
        {
            string mutexId = String.Format("{{{0}}}", "NazariiLeut");
            Mutex mutex = new Mutex(false, mutexId);
            Random rnd = new Random();
            int[] arr = new int[30];

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(10, 100);
            }

            mutex.WaitOne();
            FileWriteLineByLine(arr);
            mutex.ReleaseMutex();
        }
    }
}
