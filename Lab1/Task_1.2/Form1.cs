﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_1._2
{
    public partial class MainWindow : Form
    {
        Mutex mutex;

        public MainWindow()
        {
            InitializeComponent();

            string mutexId = String.Format("{{{0}}}", "NazariiLeut");
            mutex = new Mutex(false, mutexId);

            mutex.WaitOne();
            ReadAndVisualize();
            mutex.ReleaseMutex();

            Task.Run(async () =>
            {
                for (;;)
                {
                    await Task.Delay(500);

                    mutex.WaitOne();
                    ReadAndVisualize();
                    mutex.ReleaseMutex();
                }
            });
        }

        private void ReadAndVisualize()
        {
            var lines = File.ReadLines("../../../data.dat", Encoding.UTF8);

            List<string> list = new List<string>();

            foreach (var l in lines)
            {
                if (!int.TryParse(l, out int amount))
                    break;

                string line = "";
                for (int i = 0; i < amount; i++)
                {
                    line += "*";
                }

                list.Add(line);
            }

            listBoxData.DataSource = list;
        }

        private void buttonSort_Click(object sender, EventArgs e)
        {

        }
    }
}
